//	显示错误信息

//	验证手机号
function check_tel(){
	var tel = $.trim($(".tel").val());
	var isMobile = /^1[34578]\d{9}$/;
	$(".tel").focus(function(){
		$(".tel-error").hide();
	});
	if(!tel || !isMobile.test(tel)){
		$(".tel-error").show();
		return false;
	}
	return true;
}
//	验证旧密码
function check_opwd(){
	var opwd = $(".pwd-old").val();
	$(".pwd-old").focus(function(){
		$(".pld-error").hide();
	});
	if(!opwd || opwd.length < 6 || opwd.length > 18){
		$(".pld-error").show();
		return false;
	}
	return true;
}
//	验证密码
function check_pwd(){
	var pwd = $(".pwd1").val();
	$(".pwd1").focus(function(){
		$(".pwd1-error").hide();
	});
	if(!pwd || pwd.length < 6 || pwd.length > 18){
		$(".pwd1-error").show();
		return false; 
	}
	return true;
}
//	验证确认密码
function check_rpwd(){
	var pwd1 = $(".pwd1").val();
	var pwd2 = $(".pwd2").val();
	$(".pwd2").focus(function(){
		$(".pwd2-error").hide();
	})
	if(!pwd2 || (pwd1 != pwd2)){
		$(".pwd2-error").show();
		return false;
	}
	return true;
}
//	验证验证码
function check_code(){
	var code = $(".code").val();
	$(".code").focus(function(){
		$('.code-error').hide();
	});
	if(!code || code.length > 6){
		$(".code-error").show();
		return false;
	}
	$.ajax({
		type:"post",
		url:"",
		async:true,
		data:{"code":code},
		success:function(data){
			if(code != data.code){
				$(".code-error").show();
				return false;
			}
		}
	});
	return true;
}

//	验证身份证
function check_card(){
	var card = $(".card").val();
	//	15位身份证
//	var isIDCard1=/^[1-9]\d{7}((0\d)|(1[0-2]))(([0|1|2]\d)|3[0-1])\d{3}$/;
	//	18位身份证
	var isIDCard2=/^[1-9]\d{5}[1-9]\d{3}((0\d)|(1[0-2]))(([0|1|2]\d)|3[0-1])\d{4}$/;
	
	$(".card").focus(function(){
		$(".card-error").hide();
	});
	
	if(!card || !isIDCard2.test(card)){
		$(".card-error").show();
		return false;
	}
	return true;
}

//	验证姓名
function check_name(){
	var username = $(".username").val();
	var isName = /^[\u4E00-\u9FA5]{1,6}$/;
	$(".username").focus(function(){
		$(".name-error").hide();
	})
	if(!username || !isName.test(username)){
		$(".name-error").show();
		return false;
	}
	return true;
}

//	登录页面验证
function check_login(){
	if(!check_tel()){
		return false;
	}else if(!check_pwd()){
		return false;
	}else{
		$.ajax({
			url:'/',
			type:'post',
			dataType:'json',
			data:{
				"tel":tel,
				"pwd":pwd
			},
			success:function(data){
				if(tel != data.tel){
					$(".tel-error").show().html("*改手机号未注册");
					return false;
				}else if(pwd != data.pwd){
					$(".pwd1-error").show();
					return false;
				}
			}
		});
	}	
}

//	设置密码验证
function check_register1(){
	if(!check_code()){
		return false;
	}else if(!check_pwd()){
		return false;
	}else if(!check_rpwd()){
		return false;
	}else if(!$(".checkbox").is(":checked")){
		return false;
	}
	$(".set-psd").hide();
	$(".rel-name").show();
	$(".navtop li").eq(1).removeClass("active");
	$(".navtop li").eq(2).addClass("active");
	return false;
}

//	注册页面验证
function check_register(){
	if(!check_tel()){
		return false;
	}
	$.ajax({
		type:'post',
		url:'',
		dataType:'json',
		data:{"tel":tel},
		success:function(data){
			if(tel == data.tel){
				$(".tel-error").show().html("该手机号已被注册，请直接登录");
				return false;
			}else{
				$(".tab-content").hide();
				$(".set-psd").show();
				$(".navtop li").eq(0).removeClass("active");
				$(".navtop li").eq(1).addClass("active");
				return false;
			}
		}
	});
	check_register1();
	if(!check_name()){
		return false;
	}else if(!check_card()){
		return false;
	}
	
}

//	忘记密码页面验证
function check_forgetPwd(){
	if(!check_tel()){
		return false;
	}
	$.ajax({
		type:'post',
		url:'',
		async:true,
		dataType:'json',
		data:{"tel":tel},
		success:function(data){
			if(tel != data.tel){
				$(".tel-error").show().html("该手机号未注册，请先注册"); 
				return false;
			}else{
				$(".forget1").hide();
				$(".forget2").show();
			}
		}
	});
	if(!check_code()){
		return false;
	}else if(!check_pwd()){
		return false;
	}else{
		$(".wrap,.change-ok").show();
		setTimeout(function(){
			$(".wrap,.change-ok").hide();
			window.location.href="/wechat/index.html";
		},2000)
	}
	
}

//	个人账户，实名认证
function per_name(){
	window.location.href="/wechat/register.html";
	$(".tab-content").hide();
	$(".rel-name").show();
}

// 更换手机号
function change_tel(){
	if(!check_tel()){
		return false;
	}else{
		$.ajax({
		type:'post',
		url:'',
		async:true,
		dataType:'json',
		data:{"tel":tel},
		success:function(data){
			if(tel == data.tel){
				$(".tel-error").show().html("该手机号与当前绑定的手机号相同");
				return false;
			}else{
				window.location.href="/wechat/txyzm.html";
			}
		}
	});
	}
}

// 填写验证码
function inp_code(){
	if(!check_code()){
		return false;
	}else{
		$(".wrap,.change-ok").show();
		setTimeout(function(){
			$(".wrap,.change-ok").hide();
			window.location.href="/wechat/personal.html";
		},2000)
	}
}

// 修改密码
function change_pwd(){
	if(!check_opwd()){
		return false;
	}else{
		$.ajax({
			url:'/',
			type:'post',
			dataType:'json',
			data:{"opwd":opwd},
			success:function(data){
				if(opwd != data.opwd){
					$(".pld-error").show();
					return false;
				}else if(!check_pwd()){
					return false;
				}else if(!check_rpwd()){
					return false;
				}else{
					$(".wrap,.change-ok").show();
					setTimeout(function(){
						$(".wrap,.change-ok").hide();
						window.location.href="/wechat/personal.html";
					},2000)
				}
			}
		});
	}
}

//	获取验证码
function get_code(){
	if(!check_tel()){
		return false;
	}
	var InterValObj; //timer变量，控制时间
    var count = 60; //间隔函数，1秒执行
    var curCount;//当前剩余秒数
	var tel = $.trim($(".tel").val());
	$.ajax({
		type:"post",
		dataType:"json",
		url:"/wechat/get_code.html",
		data:{"tel":tel},
		success:function(data){
			if(data.code == true){
				curCount = count;
				$(".reget").attr("disabled","true");
				$(".reget").val(curCount+"s重新获取");
				InterValObj = setInterval(function(){
					if(curCount == 0){
						clearInterval(InterValObj);	//	停止计时器
						$(".reget").val("重新获取");
					}else{
						count--;
						$(".reget").val(curCount+"s重新获取");
					} 
				}, 1000); //启动计时器，1秒执行一次
			}
		}
	})
}

//显示删除按钮
function show_remove(){
	var val = $("input").val();
	if(val){
		$(".remove").show();
	}
}


$(function(){
	//	注册页面tab切换
	$(".navtop li").click(function(){
		$(this).addClass("active").siblings("li").removeClass("active");
		var index = $(this).index();
		$(".tab-content").eq(index).show().siblings(".tab-content").hide();
	});
	
})

 