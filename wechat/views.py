from django.shortcuts import render,HttpResponse
import json,time
from urllib import request as req
# Create your views here.

def index(request):
    return render(request,'index.html')

def register(request):
    if request.method == 'POST':
        #return HttpResponse(request.POST['tel'])
        tran_time = time.time()
        tran_date = time.strftime('%Y-%m-%d',time.localtime(time.time()))
        data = {
            'head':{
                'tran_id':'8003',
                'tran_date': tran_date,
                'tran_time': tran_time,
            },
            # 'body':{
            #     'phone':str(request.POST['tel']),
            #     'password':request.POST['pwd'],
            #     'name':request.POST['name'],
            #     'idcard':request.POST['card'],
            #     'wx_id':'12121',
            # }
            'body':{
                'phone':'18751970618',
                'password':'test123',
                'name':'sxy',
                'idcard':'130726198105290019',
                'wx_id':'test',
            }
        }
        data = json.dumps(data).encode(encoding='utf-8')
        re = req.Request(url='http://192.168.86.132:9092/web/orderservlet',data=data)
        res = json.loads(req.urlopen(re).read().decode())
        if res['rc'] == 0:
            ret = res['rc']
            return HttpResponse(ret)
        else:
            ret = res['msg']
            return HttpResponse(ret)
    else:
        return render(request,'register.html')


def personal(request):
    return render(request,'personal.html')

def yypd(request):
    return render(request,'yypd.html')

def wjmm(request):
    return render(request,'wjmm.html')

def address(request):
    return render(request,'address.html')

def login(request):
    data = {
        'head':{
            'tran_id': '8001',
            'tran_date': '2017112',
            'tran_time': '147460654',
        },
        'body':{
            'phone': '13951112111',
            'password': '123123',
        }

    }
    data = json.dumps(data).encode(encoding='utf-8')
    re = req.Request(url='http://www.sino-device.com.cn:9040/web/robotservlet',data=data)
    res = json.loads(req.urlopen(re).read().decode())
    if res['rc'] == 0:
        return render(request,'personal.html')
    else:
        return render(request, 'index.html')


def changetel(request):
    return render(request,'change-tel.html')

def xgmm(request):
    return render(request,'xgmm.html')

def get_code(request):
    tel = request.POST['tel']
    if tel == 18751970618:
        res = {'msg':'改手机号已经注册','msg_code':0}
    else:
        res = {'code':1234,'msg_code':1}
    return HttpResponse(json.dumps(res),content_type='application/json')
