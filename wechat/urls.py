from django.conf.urls import url
from wechat import views
urlpatterns = [
    url(r'^index.html/',views.index), #首页
    url(r'^register.html/',views.register), #注册页
    url(r'^personal.html/',views.personal), #个人中心
    url(r'^yypd.html/',views.yypd), #预约排队
    url(r'^wjmm.html/',views.wjmm), #忘记密码
    url(r'^address.html/',views.address), #地图
    url(r'^login.html/',views.login), #登陆
    url(r'^changetel.html/',views.changetel), #修改手机号码
    url(r'^yyzs.html/',views.changetel), #预约展示
    url(r'^xgmm.html/',views.changetel), #修改密码
    url(r'^get_code.html/',views.get_code),#获取验证码
]